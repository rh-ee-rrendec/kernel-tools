#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-2.0-or-later

import subprocess
import argparse
import sys

try:
    import urwid
except ImportError as e:
    print(f"Error: {e}", file=sys.stderr)
    print("Try `pip3 install urwid`", file=sys.stderr)
    sys.exit(1)

palette = [
    ('green', 'dark green', 'default'),
    ('green_highlight', 'black', 'dark green'),
    ('red', 'dark red', 'default'),
    ('red_highlight', 'black', 'dark red'),
    ('chunk-header', 'dark cyan', 'default'),
    ('diff-files', 'bold', 'default'),
    ('diff-files_highlight', 'bold,standout', 'default'),
    ('highlight', 'standout', 'default'),
    ('current_item', 'black', 'light gray'),
    ('selected_item', 'white', 'light red'),
    ('footer', 'black', 'white')
]

class DiffsList(urwid.ListBox):
    def __init__(self, diffs, select_callback):
        self.selected_item = None
        self.select_callback = select_callback
        super(DiffsList, self).__init__([DiffItem(n, d) for n,d in diffs])
        urwid.connect_signal(self.body, 'modified', self.select_item)
        self.select_item()

    def select_item(self):
        if self.selected_item is not None:
            self.selected_item.set_attr_map({None: None})
        self.selected_item = self.focus
        self.selected_item.set_attr_map({None: 'selected_item'})
        self.select_callback(self.selected_item.diff_name)

class DiffItem(urwid.AttrMap):
    def __init__(self, diff_name, diff_description):
        self.diff_name = diff_name
        text_item = urwid.Text(diff_description, wrap='ellipsis')
        text_item._selectable = True
        text_item.keypress = self.keypress
        super(DiffItem, self).__init__(text_item, 'list_item', 'current_item')

    def keypress(self, size, key):
        if key == 'enter':
            enter_diff_viewer()
        else:
            return key

class DiffViewer(urwid.ListBox):
    def __init__(self, diff_type='commit', content=[]):
        self.diff_name = None  # either a commit hash or a file path
        self.search_results = []
        if diff_type == 'commit':
            self.open_diff = self.open_commit
        else:
            self.open_diff = self.open_patch
        super(DiffViewer, self).__init__(content)

    def open_commit(self, commit_hash):
        if commit_hash != self.diff_name:  # avoid scroll to the top if already open
            rc, text = subprocess.getstatusoutput(f"git show {commit_hash}")
            if rc == 0:
                self.set_content(commit_hash, text)
            else:
                self.set_content(commit_hash, f"can't open {commit_hash}!!!")

    def open_patch(self, patch_path):
        if patch_path != self.diff_name:  # avoid scroll to the top if already open
            with open(patch_path) as f:
                text = f.read()
            self.set_content(patch_path, text)

    def set_content(self, diff_name, text):
        lines = text.splitlines()
        fmtd_lines = []
        i = 0

        while i < len(lines):
            line = lines[i].expandtabs(8)
            if line.startswith("+"):
                fmtd_lines.append(('green', line))
            elif line.startswith("-"):
                fmtd_lines.append(('red', line))
            elif line.startswith("diff"):
                fmtd_lines.append('\n')
                while i < len(lines) and not lines[i].startswith("@@"):
                    fmtd_lines.append(('diff-files', lines[i]))
                    i += 1
                i -= 1
            elif line.startswith("@@"):
                end = 2 + line.find("@@", 2)
                fmtd_lines.append([('chunk-header', line[:end]), line[end:]])
            else:
                fmtd_lines.append(line)
            i += 1

        self.diff_name = diff_name
        self.body.clear()
        self.body.extend(DiffLine(fmtd_line) for fmtd_line in fmtd_lines)

    def search(self, search_text):
        self.search_results.clear()

        for i, diff_line in enumerate(self.body):
            diff_line.set_search_highlights(search_text)
            if search_text and search_text in diff_line.text:
                self.search_results.append(i)

        self.search_next()

    def search_next(self):
        if self.search_results:
            self.focus_position = next((pos for pos in self.search_results if pos > self.focus_position),
                                       self.search_results[0])
            self.set_focus_valign('top')

    def search_prev(self):
        if self.search_results:
            self.focus_position = next((pos for pos in reversed(self.search_results) if pos < self.focus_position),
                                       self.search_results[-1])
            self.set_focus_valign('top')

    def keypress(self, size, key):
        if key == 'esc':
            exit_diff_viewer()
        elif key == '/':
            open_search(self.search)
        elif key == 'n':
            self.search_next()
        elif key == 'N':
            self.search_prev()
        else:
            return super(DiffViewer, self).keypress(size, key)

class DiffLine(urwid.Text):
    def __init__(self, markup):
        super(DiffLine, self).__init__(markup)
        self.default_attribs = self.attrib

    def set_search_highlights(self, search_text):
        self._attrib = self.default_attribs

        if search_text:
            line = self.text
            style = self.attrib[0][0] if self.attrib else None

            # Here we're asuming each line have only one format (it's not
            # divided in differently formatted chunks). Otherwise, it would be
            # more complicated to calculate the formatting.
            # Exception: chunk headers have 2 parts with different format. The
            # first part only contains the "@@ -xxx,+xxx @@" that we can ignore
            # to maintain the format calculation simple
            if style != 'chunk-header':
                self.set_text(DiffLine.create_highlights_markup(line, search_text, style))
            else:
                end = 2 + line.find("@@", 2)
                part1, part2 = line[:end], line[end:]
                self.set_text([('chunk-header', part1)]
                              + DiffLine.create_highlights_markup(part2, search_text, None))
        else:
            self._invalidate()  # redraw

    def create_highlights_markup(line, search_text, style=None):
        highlight_style = style + '_highlight' if style else 'highlight'

        chunks = []
        pos = line.find(search_text)
        while pos != -1:
            end = pos + len(search_text)
            normal_text, highlight_text = line[:pos], line[pos:end]
            chunks.extend([(style, normal_text), (highlight_style, highlight_text)])

            line = line[end:]
            pos = line.find(search_text)

        chunks.append((style, line))
        return chunks

class ColumnsFocusCB(urwid.Columns):
    @urwid.Columns.focus_position.setter
    def focus_position(self, position):
        urwid.Columns.focus_position.fset(self, position)
        column_focus_changed(self.focus_position)

class SearchBox(urwid.Edit):
    def __init__(self):
        self.callback = None
        self.history = []
        self.history_idx = 0
        super(SearchBox, self).__init__('/ ')

    def keypress(self, size, key):
        if key == 'esc':
            close_search()
        elif key == 'enter':
            self.confirm_search()
        elif key == 'up':
            self.history_up()
        elif key == 'down':
            self.history_down()
        else:
            return super(SearchBox, self).keypress(size, key)

    def confirm_search(self):
        self.history.append(self.get_edit_text())
        self.history_idx = len(self.history) - 1
        cback, self.callback = self.callback, None

        close_search()
        if cback:
            cback(self.get_edit_text())

    def history_up(self):
        if self.history_idx > 0:
            self.history_idx -= 1
            self.set_edit_text(self.history[self.history_idx])

    def history_down(self):
        if self.history_idx < len(self.history) - 1:
            self.history_idx += 1
            self.set_edit_text(self.history[self.history_idx])

def enter_diff_viewer():
    body.focus_position = 1

def exit_diff_viewer():
    body.focus_position = 0

def column_focus_changed(focus_position):
    if focus_position == 0:
        help_text.set_text("<q> quit")
    else:
        help_text.set_text("<q> quit, </> search, <esc> back, <n> search next, <N> search prev")

def open_search(search_callback):
    search_box.callback = search_callback
    window.contents['footer'] = (search_box_wrapper, None)
    window.focus_position = 'footer'

def close_search():
    search_box.callback = None
    window.contents['footer'] = (help_text_wrapper, None)
    window.focus_position = 'body'

def key_handler(key):
    if key in ('q', 'Q'):
        raise urwid.ExitMainLoop()
    return key  # unhandled

def list_file_iter(list_path):
    with open(list_path) as f:
        for line in [l.strip() for l in f]:
            if line and not line.startswith('#'):
                yield line

def read_commits_list_file(list_path):
    commits = []
    for line in list_file_iter(list_path):
        commit_hash = line.split()[0]
        rc, commit_msg = subprocess.getstatusoutput(f"git log --oneline -1 {commit_hash}")
        if rc != 0:
            commit_msg = f"{commit_hash} <error: can't get commit message>"
        commits.append((commit_hash, commit_msg))
    return commits

def read_patches_list_file(list_path):
    return [(patch, patch) for patch in list_file_iter(list_path)]

def read_quilt_series():
    rc, quilt_series = subprocess.getstatusoutput("quilt series")
    if rc != 0:
        raise Exception(f"error: <`quilt series` error({rc}): {quilt_series}>")
    return [(patch, patch) for patch in quilt_series.splitlines()]

def args_parse():
    description="TUI application to see the content of a list of commits or patches."
    epilog=\
"""Unlike other applications that focus on Git history, where all the commits
have to belong to the same history ranges, this program allows to visualize a
list of random commits that doesn't to be related to each other."""

    parser = argparse.ArgumentParser(description=description, epilog=epilog, formatter_class=argparse.RawDescriptionHelpFormatter )
    mode_group = parser.add_mutually_exclusive_group()
    mode_group.add_argument('-g', '--git', metavar='FILE', help="Read FILE as list of commits in current repo")
    mode_group.add_argument('-p', '--patches', metavar='FILE', help="Read FILE as list of patch files")
    mode_group.add_argument('-q', '--quilt', action='store_true', help="Get list of patches from `quilt series`")
    return parser.parse_args(None if sys.argv[1:] else ["--help"])

def get_diffs(args):
    try:
        if args.git:
            return read_commits_list_file(args.git)
        elif args.patches:
            return read_patches_list_file(args.patches)
        elif args.quilt:
            return read_quilt_series()
        else:
            raise Exception("Required arg -g|-p|-q")
    except Exception as e:
        print(e)
        sys.exit(1)

if __name__ == "__main__":
    args = args_parse()
    diffs = get_diffs(args)

    # footer: help text and search box
    help_text = urwid.Text("")
    help_text_wrapper = urwid.AttrMap(help_text, 'footer')
    search_box = SearchBox()
    search_box_wrapper = urwid.AttrMap(search_box, 'footer')

    # body: diffs list and diff viewer
    diff_viewer = DiffViewer('commit' if args.git else 'patch')
    diffs_list = DiffsList(diffs, diff_viewer.open_diff)
    body = ColumnsFocusCB([('weight', 0.25, diffs_list), ('weight', 0.75, diff_viewer)], dividechars=2)

    # put all together and run main loop
    window = urwid.Frame(body, footer=help_text_wrapper)
    urwid.MainLoop(window, palette, unhandled_input=key_handler).run()
